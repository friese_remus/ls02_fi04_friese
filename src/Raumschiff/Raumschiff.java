package Raumschiff;


import java.util.List;

public class Raumschiff {
    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffsname;
    private List<String> broadcastKommunikator;
    private List<Ladung> ladung;

    public Raumschiff() {
    }

    public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname, List<String> broadcastKommunikator, List<Ladung> ladung) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schildeInProzent = schildeInProzent;
        this.huelleInProzent = huelleInProzent;
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.androidenAnzahl = androidenAnzahl;
        this.schiffsname = schiffsname;
        this.broadcastKommunikator = broadcastKommunikator;
        this.ladung = ladung;
    }

    //addLadung
    public void beladen(Ladung ladung){
        this.ladung.add(ladung);
    }

    public void zustandAusgeben() {
        System.out.print( "Raumschiff{" +
                "photonentorpedoAnzahl=" + this.photonentorpedoAnzahl +
                ",\nenergieversorgungInProzent=" + this.energieversorgungInProzent +
                ",\nschildeInProzent=" + this.schildeInProzent +
                ",\nhuelleInProzent=" + this.huelleInProzent +
                ",\nlebenserhaltungssystemeInProzent=" + this.lebenserhaltungssystemeInProzent +
                ",\nandroidenAnzahl=" + this.androidenAnzahl +
                ",\nschiffsname='" + this.schiffsname + '\'' +
                ",\nbroadcastKommunikator=" + this.broadcastKommunikator +
                '}');
        this.ladeverzeichnisAusgeben();
    }

    public void ladeverzeichnisAusgeben(){
        for(Ladung ladung:this.ladung){
            System.out.println("ladung=" + ladung);
        }
    }

    public void photonentorpedosAbschießen(Raumschiff Raumschiff){
        if (this.photonentorpedoAnzahl < 1){
            this.nachrichtSenden("-=*Click*=-");
        }
        else{
            this.photonentorpedoAnzahl--;
            this.nachrichtSenden("Photonentorpedo abgeschossen");
            this.trefferVermerken(Raumschiff);
        }
    }

    public void phaserkanoneAbschießen(Raumschiff Raumschiff){
        if (this.energieversorgungInProzent < 50){
            this.nachrichtSenden("-=*Click*=-");
        }
        else{
            this.energieversorgungInProzent -= 50;
            this.nachrichtSenden("Phaserkanone abgeschossen");
            this.trefferVermerken(Raumschiff);
        }
    }

    private void trefferVermerken(Raumschiff Raumschiff){
        this.nachrichtSenden(Raumschiff.schiffsname + "wurde getroffen!");
        Raumschiff.schildeInProzent -= 50;
        if(Raumschiff.schildeInProzent < 1){
            Raumschiff.huelleInProzent -=50;
            Raumschiff.energieversorgungInProzent -=50;
            if(Raumschiff.huelleInProzent < 1){
                Raumschiff.lebenserhaltungssystemeInProzent = 0;
                this.nachrichtSenden("Lebenserhaltungssysteme" + Raumschiff.schiffsname + "wurden vernichtet");
            }
        }
    }

    //Nachrichten an Alle
    public void nachrichtSenden(String nachricht){
        this.broadcastKommunikator.add(nachricht);

        System.out.println(nachricht);
    }

    //zurückgegeben wird ja durch getter, daher hier als ausgeben interpretiert
    public void logbuchZurückgeben(){
        for(String nachricht:this.broadcastKommunikator){
            System.out.println(nachricht);
        }
    }

    //Ladung “Photonentorpedos” einsetzen
    public void torpedorohreLaden(int anzahl){
        for(Ladung ladung:this.ladung){
            if (ladung.getBezeichnung() == "Photonentorpedo"){
                if(ladung.getMenge() < anzahl){
                    anzahl = ladung.getMenge();
                }
                ladung.setMenge(ladung.getMenge() - anzahl);
                this.photonentorpedoAnzahl += anzahl;
                System.out.println(anzahl + "Photonentorpedo(s) eingesetzt");

                return;
            }
        }
        System.out.println("Keine Photonentorpedos gefunden!");
        this.nachrichtSenden("-=*Click*=-");
    }

    public void ladeverzeichnisAufräumen(){
        for(Ladung ladung:this.ladung){
            if(ladung.getMenge() < 1){
                this.ladung.remove(ladung);
            }
        }
    }

    public int getPhotonentorpedoAnzahl() {
        return photonentorpedoAnzahl;
    }

    public int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }

    public int getSchildeInProzent() {
        return schildeInProzent;
    }

    public int getHuelleInProzent() {
        return huelleInProzent;
    }

    public int getLebenserhaltungssystemeInProzent() {
        return lebenserhaltungssystemeInProzent;
    }

    public int getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    public String getSchiffsname() {
        return schiffsname;
    }

    public List<String> getBroadcastKommunikator() {
        return broadcastKommunikator;
    }

    public List<Ladung> getLadung() {
        return ladung;
    }

    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    }

    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }

    public void setSchildeInProzent(int schildeInProzent) {
        this.schildeInProzent = schildeInProzent;
    }

    public void setHuelleInProzent(int huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }

    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
    }

    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }

    public void setBroadcastKommunikator(List<String> broadcastKommunikator) {
        this.broadcastKommunikator = broadcastKommunikator;
    }

    public void setLadung(List<Ladung> ladung) {
        this.ladung = ladung;
    }
}
