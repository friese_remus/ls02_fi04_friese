package kreis;

import java.awt.Point;

public class Rechteck {
    private double width;
    private double height;
    private Point origin;

    public Rechteck(double width, double height, Point origin)
    {
        setOrigin(origin);
        setWidth(width);
        setHeight(height);
    }
    public void setWidth(double width) {
        if(width > 0)
            this.width = width;
        else
            this.width = 0;
    }
    public double getWidth() {
        return this.width;
    }
    public void setHeight(double height) {
        if(height > 0)
            this.height = height;
        else
            this.height = 0;
    }
    public double getHeight() {
        return this.height;
    }
    public double getFlaeche() {
        return this.height * this.width;
    }
    public double getUmfang() {
        return (2 * this.height) + (2 * this.width);
    }
    public double getDiagonale() {
        return Math.sqrt((this.width * this.width) + (this.height * this.height));
    }
    public void setOrigin(Point origin) {
        this.origin = origin;
    }
    public Point getOrigin() {
        return origin;
    }
}
