package kreis;

import java.awt.Point;

public class TestRechteck {
    public static void main(String[] args) {

        Rechteck r = new Rechteck(5, 10, new Point(3,4));
        System.out.println("Fläche: " + r.getFlaeche());
        System.out.println("Umfang: " + r.getUmfang());
        System.out.println("Diagonale: " + r.getDiagonale());

    }
}
